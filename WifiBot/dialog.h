#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <myrobot.h>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

private slots:
    void on_pushButtonA_pressed();

    void on_pushButtonD_pressed();

    void on_pushButtonR_pressed();

    void on_pushButtonG_pressed();

    void on_pushButtonC_clicked();

    void on_pushButtonD_2_clicked();

private:
    Ui::Dialog *ui;
    MyRobot monRobot;
};

#endif // DIALOG_H
