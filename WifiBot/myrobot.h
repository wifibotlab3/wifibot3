#ifndef CONNEXION_H
#define CONNEXION_H
#ifndef MYROBOT_H
#define MYROBOT_H

#include <QObject>
#include <QTcpSocket>
#include <QAbstractSocket>
#include <QDebug>
#include <QTimer>
#include <QMutex>



class MyRobot : public QObject {

    Q_OBJECT
public:
    explicit MyRobot(QObject *parent);
    void doConnect(QString adresseIP, unsigned short port);
    void disConnect();
    QByteArray DataToSend;
    QByteArray DataReceived;
    QMutex Mutex;
    //variable pour stocker les valeurs du robot
    int valeurSpeedAvDroit;
    int valeurSpeedArDroit ;
    int progressBar ;
    int valeurSensors_1 ;
    int valeurSensors_2 ;
    int odometrie_1 ;
    int odometrie_2 ;
    int odometrie_3 ;
    int odometrie_4 ;
    int valeurSpeedAvGau;
    int valeurSpeedArGau ;
    int valeurSensors_3 ;
    int valeurSensors_4 ;
    int valeurRobot ;
    int valeurVersion ;

signals:
    void updateUI(const QByteArray Data);
public slots:
    void connected();
    void deplacer(int roueAvantGauche, int roueArriereGauche, int roueAvantDroit, int roueArriereDroit, int direction);
    short Crc16(QByteArray *DataToSend , unsigned char Taille_max);
    void disconnected();
    void bytesWritten(qint64 bytes);
    void readyRead();
    void MyTimerSlot();

private:
    QTcpSocket *socket;
    QTimer *TimerEnvoi;

};

#endif // MYROBOT_H

#endif // CONNEXION_H
