#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QKeyEvent>
#include <QWebEngineView>
#include <QTimer>
#include <QMutex>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    //permet de recuperer les touches du clavier
    void keyPressEvent(QKeyEvent *event);
    ~MainWindow();
    //on l'utilise pour le timer
    QMutex Mutex;


private slots:
    void on_dial_actionTriggered(int action);

    void on_ConnexionpushButton_clicked();

    void on_DeconnectionpushButton_2_clicked();

    void on_cameraUp_clicked();

    void on_cameraRight_clicked();

    void on_manetteUp_clicked();

    void on_plainTextEdit_textChanged();

    void on_manetteUp_pressed();

    void on_manetteRight_pressed();

    void on_manetteRight_clicked(bool checked);

    void on_manetteUp_clicked(bool checked);

    void on_manetteDown_clicked();

    void on_SpeedControl_valueChanged(int value);

    void on_manetteRight_clicked();

    void on_manetteLeft_clicked();

    void maTimer();

private:
    Ui::MainWindow *ui;
    //variable qui contient la camera
    QWebEngineView* view;
    //un  Timer
    QTimer *Timere;

};

#endif // MAINWINDOW_H
