SOURCES += \
    lemain.cpp \
    mainwindow.cpp \
    myrobot.cpp

HEADERS += \
    mainwindow.h \
    myrobot.h

QT += widgets
QT += network
QT += core gui webenginewidgets \
                 multimedia
FORMS += \
    mainwindow.ui
