#include "dialog.h"
#include "ui_dialog.h"
#include "myrobot.h"


Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    monRobot = new MyRobot(this);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButtonA_pressed()
{

}

void Dialog::on_pushButtonD_pressed()
{

}

void Dialog::on_pushButtonR_pressed()
{

}

void Dialog::on_pushButtonG_pressed()
{

}

void Dialog::on_pushButtonC_clicked()
{
    monRobot->doConnect();

}

void Dialog::on_pushButtonD_2_clicked()
{

}
