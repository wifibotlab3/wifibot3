// myrobot.cpp

#include "myrobot.h"


MyRobot::MyRobot(QObject *parent) : QObject(parent) {
    DataToSend.resize(9);
    DataToSend[0] = 0xFF;
    DataToSend[1] = 0x07;
    DataToSend[2] = 0x0;
    DataToSend[3] = 0x0;
    DataToSend[4] = 0x0;
    DataToSend[5] = 0x0;
    DataToSend[6] = 0x0;
    DataToSend[7] = 0x0;
    DataToSend[8] = 0x0;
    DataReceived.resize(21);
    TimerEnvoi = new QTimer();
    // setup signal and slot
    connect(TimerEnvoi, SIGNAL(timeout()), this, SLOT(MyTimerSlot())); //Send data to wifibot timer
}


void MyRobot::doConnect(QString adresseIP, unsigned short port) {
    socket = new QTcpSocket(this); // socket creation
    connect(socket, SIGNAL(connected()),this, SLOT(connected()));
    connect(socket, SIGNAL(disconnected()),this, SLOT(disconnected()));
    connect(socket, SIGNAL(bytesWritten(qint64)),this, SLOT(bytesWritten(qint64)));
    connect(socket, SIGNAL(readyRead()),this, SLOT(readyRead()));
    qDebug() << "connecting..."; // this is not blocking call
    //socket->connectToHost("LOCALHOST", 15020);
    //192.168.1.10
    socket->connectToHost(adresseIP, port); // connection to wifibot
    // we need to wait...
    if(!socket->waitForConnected(5000)) {
        qDebug() << "Error: " << socket->errorString();
        return;
    }
    TimerEnvoi->start(75);

}

void MyRobot::deplacer(int roueAvantGauche, int roueArriereGauche, int roueAvantDroit, int roueArriereDroit, int direction) {
    qDebug() << "avancer..."; // Hey server, tell me about you.

    DataToSend.resize(9);
    DataToSend[0] = 0xFF;
    //taille du tableau
    DataToSend[1] = 0x07;
    //vitesse roues gauche
    DataToSend[2] = roueAvantGauche;
    DataToSend[3] = roueArriereGauche;
    //vitesse roues droit
    DataToSend[4] = roueAvantDroit;
    DataToSend[5] = roueArriereDroit;
    //Option (avancer, reculer, ....)
    DataToSend[6] = direction;
    //calcul du crc
    short crc = Crc16(&DataToSend,6);
    DataToSend[7] = crc;
    //le crc est sur 16 bit donc on fait un de 8 bit car chaque case est sur 8 bit
    DataToSend[8] = (crc >> 8);

    //donnee qu'on recoit
    //DataReceived.resize(21);

    //TimerEnvoi = new QTimer();
    // setup signal and slot
    //connect(TimerEnvoi, SIGNAL(timeout()), this, SLOT(MyTimerSlot())); //Send data to wifibot timer

}

void MyRobot::disConnect() {
    TimerEnvoi->stop();
    socket->close();
}

void MyRobot::connected() {
    qDebug() << "connected..."; // Hey server, tell me about you.
}

void MyRobot::disconnected() {
    qDebug() << "disconnected...";
}

void MyRobot::bytesWritten(qint64 bytes) {
    qDebug() << bytes << " bytes written...";
}

void MyRobot::readyRead() {
    qDebug() << "reading..."; // read the data from the socket

    //recuperation des infos du robot

    DataReceived = socket->readAll();
    emit updateUI(DataReceived);
    qDebug() << DataReceived[0] << DataReceived[1] << DataReceived[2];
    this->valeurSpeedAvDroit  = DataReceived[0];
    this->valeurSpeedArDroit  = DataReceived[1];
    this->progressBar  = DataReceived[2];
    this->valeurSensors_1  = DataReceived[3];
    this->valeurSensors_2  = DataReceived[4];
    this->odometrie_1  = DataReceived[5];
    this->odometrie_2  = DataReceived[6];
    this->odometrie_3  = DataReceived[7];
    this->odometrie_4  = DataReceived[8];
    this->valeurSpeedAvGau = DataReceived[9];
    this->valeurSpeedArGau  = DataReceived[10];
    this->valeurSensors_3  = DataReceived[11];
    this->valeurSensors_4  = DataReceived[12];
    this->valeurRobot  = DataReceived[17];
    this->valeurVersion  = DataReceived[18];


}

void MyRobot::MyTimerSlot() {
    qDebug() << "Timer...";
    while(Mutex.tryLock());
    socket->write(DataToSend);
    Mutex.unlock();
}

short MyRobot::Crc16(QByteArray *DataToSend, unsigned char Taille_max)
{
    unsigned char *Adresse_tab = ((unsigned char*) DataToSend->data());
    unsigned int Crc = 0xFFFF;
    unsigned int Polynome = 0xA001;
    unsigned int CptOctet = 0;
    unsigned int CptBit = 0;
    unsigned int Parity= 0;

    Crc = 0xFFFF;
    Polynome = 0xA001;

    for ( CptOctet= 1 ; CptOctet < Taille_max+1 ; CptOctet++)
    {
        Crc ^= *( Adresse_tab + CptOctet);

        for ( CptBit = 0; CptBit <= 7 ; CptBit++)
        {
            Parity= Crc;
            Crc >>= 1;

            if (Parity%2 == true) Crc ^= Polynome;
        }
    }

    return(Crc);
}
