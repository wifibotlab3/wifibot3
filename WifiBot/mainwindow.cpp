#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "myrobot.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),

ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setStyleSheet("{background-image: url(:/images/milky-way-2695569_960_720.jpg.jpg);}");
    ui->AdresseIP->setText("192.168.1.10");
    ui->port->setText("15020");
    ui->progressBar->setValue(50);
    Timere = new QTimer();
    //appel du Timer
    connect(Timere, SIGNAL(timeout()), this, SLOT(maTimer()));

}

QObject *unparent = 0;
MyRobot *monRobot = new MyRobot(unparent);
int vpeedControl = 300;

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_dial_actionTriggered(int action)
{

}

void MainWindow::maTimer()
{
    while(Mutex.tryLock());
    //affichage des innfos du robot grace au mutex et au timer
    ui->ValeurSpeedAvDroit->display(monRobot->valeurSpeedAvDroit);
    ui->ValeurSpeedArDroit->display(monRobot->valeurSpeedArDroit);
    ui->progressBar->setValue(monRobot->progressBar);
    //ui->progressBar->se
    ui->ValeurSensors_1->display(monRobot->valeurSensors_1);
    ui->ValeurSensors_2->display(monRobot->valeurSensors_2);
    ui->Odometrie_1->display(monRobot->odometrie_1);
    ui->Odometrie_2->display(monRobot->odometrie_2);
    ui->Odometrie_3->display(monRobot->odometrie_3);
    ui->Odometrie_4->display(monRobot->odometrie_4);
    ui->ValeurSpeedAvGau->display(monRobot->valeurSpeedAvGau);
    ui->ValeurSpeedArGau->display(monRobot->valeurSpeedArGau);
    ui->ValeurSensors_3->display(monRobot->valeurSensors_3);
    ui->ValeurSensors_4->display(monRobot->valeurSensors_4);
    ui->ValeurRobot->display(monRobot->valeurRobot);
    ui->ValeurVersion->display(monRobot->valeurVersion);

    Mutex.unlock();
}

void MainWindow::on_ConnexionpushButton_clicked()
{
    qDebug() << ui->AdresseIP->text();

    qDebug() << ui->port->text().toShort(0,10);

    ui->AdresseIP->text();

    monRobot->doConnect(ui->AdresseIP->text(), ui->port->text().toUShort(0,10));

    //affichage de la camera
    this->view = new QWebEngineView(this);
    this->view->load(QUrl("http://"+ui->AdresseIP->text()+":8080/?action=stream"));
    this->view->resize(610,360);
    this->view->move(278,69);
   this->view->show();


    qDebug() << monRobot->valeurSpeedAvDroit;

    //demarrage du Timer
    Timere->start(75);


}

void MainWindow::on_DeconnectionpushButton_2_clicked()
{
    Timere->stop();
    monRobot->disConnect();
}

void MainWindow::on_cameraUp_clicked()
{

}

void MainWindow::on_cameraRight_clicked()
{

}

void MainWindow::on_plainTextEdit_textChanged()
{

}


void MainWindow::keyPressEvent(QKeyEvent * event )
{
    if( event->key() == Qt::Key_Up)
    {
        qDebug() << "KeyUp" ;
        monRobot->deplacer(vpeedControl,0,vpeedControl,0,0xF0);

    }

    if( event->key() == Qt::Key_Down)
    {
        qDebug() << "Key_Down" ;
        monRobot->deplacer(0,vpeedControl,0,vpeedControl,0xA0);
    }

    if( event->key() == Qt::Key_Right)
    {
        qDebug() << "Key_Right" ;
        monRobot->deplacer(vpeedControl,vpeedControl,-40,-40,0x70);
    }

    if( event->key() == Qt::Key_Left)
    {
        qDebug() << "Key_Left" ;
        monRobot->deplacer(-40,-40,vpeedControl,vpeedControl,0xD0);
    }
}

void MainWindow::on_SpeedControl_valueChanged(int value)
{
    vpeedControl = value * 5;
    ui->ValeurSpeedControl->display(value);
}

void MainWindow::on_manetteUp_pressed()
{
   // monRobot->avancer(); //Send data to wifibot timer
}

void MainWindow::on_manetteRight_pressed()
{

}

void MainWindow::on_manetteRight_clicked(bool checked)
{


}

void MainWindow::on_manetteUp_clicked(bool checked)
{

}

void MainWindow::on_manetteUp_clicked()
{
    monRobot->deplacer(vpeedControl,0,vpeedControl,0,0xF0);
}

void MainWindow::on_manetteDown_clicked()
{
    monRobot->deplacer(0,vpeedControl,0,vpeedControl,0xA0);
}

void MainWindow::on_manetteRight_clicked()
{
   monRobot->deplacer(vpeedControl,vpeedControl,-40,-40,0x70);
}

void MainWindow::on_manetteLeft_clicked()
{
    monRobot->deplacer(-40,-40,vpeedControl,vpeedControl,0x70);
}
